﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;

namespace DataBaseConnect.Entities
{
    public class Tournament
    {
        [Id(position: 0, Name = "Id")]
        [Generator(position: 1, Class = "native")]
        public long id { get; set; }

        [Property(NotNull = true)]
        public string name { get; set; }

        public DateTime date { get; set; }

        [Property(NotNull = true)]
        public int player_size { get; set; }

        //[Bag(position: 0, Name = "OurCompositions")]
        //[OneToMany(position: 1, ClassType = typeof(OurComposition))]
        //public IList<OurComposition> our_compositions { get; set; }

        [Bag(position: 0, Name = "OppTeams")]
        [OneToMany(position: 1, ClassType = typeof(OppTeam))]
        public IList<OppTeam> opp_teams { get; set; }
    }
}
