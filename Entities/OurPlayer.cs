﻿using NHibernate.Mapping.Attributes;

namespace DataBaseConnect.Entities
{
    public class OurPlayer
    {
        [Id(position: 0, Name = "Id")]
        [Generator(position: 1, Class = "native")]
        public long id { get; set; }

        [Property(NotNull = true)]
        public string name { get; set; }

        public long tournament_id { get; set; }

        //[ManyToOne(Column = "tournament_id", ForeignKey = "ourplayer_fk_tournament_id", Cascade = "all")]
        //public virtual Tournament tournament { get; set; }
    }
}
