﻿using NHibernate.Mapping.Attributes;
using System.Collections.Generic;

namespace DataBaseConnect.Entities
{
    public class OppTeam
    {
        [Id(position: 0, Name = "Id")]
        [Generator(position: 1, Class = "native")]
        public long id { get; set; }

        [Property(NotNull = true)]
        public string name { get; set; }

        public long tournament_id { get; set; }

        //[ManyToOne(Column = "tournament_id", ForeignKey = "oppteam_fk_tournament_id", Cascade = "all")]
        //[Property(NotNull = true)]
        //public Tournament tournament { get; set; }

        //[Bag(position: 0, Name = "Composition")]
        //[Key(position: 1, Column = "OppCompositionId")]
        //[OneToMany(position: 2, ClassType = typeof(OppComposition))]
        //public IList<OppComposition> composition { get; set; }
    }
}
