﻿using DataBaseConnect.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataBaseConnect.Data
{
    class DatabaseContainer
    {
        EstimationContext db;

        public DatabaseContainer()
        {
            db = new();
        }

        public Dictionary<string, string> GetValuesNames(string type)
        {
            Dictionary<string, string> ReturnValue = new Dictionary<string, string>();

            switch (type)
            {
                case "Tournament":

                    ReturnValue.Add("name",         "Tournament name:");
                    ReturnValue.Add("date",         "Tournament date:");
                    ReturnValue.Add("player_size",  "Tournament player size");

                    break;

                case "OurPlayer":

                    ReturnValue.Add("name",         "Our player name:");

                    break;

                case "OppTeam":

                    ReturnValue.Add("name",         "Opponent team name:");

                    break;

                default:

                    break;
            }

            return ReturnValue;
        }

        public long AddItem(string type, Dictionary<string, string> values)
        {
            switch (type)
            {
                case "Tournament":
                    Tournament new_tournament = new Tournament();

                    new_tournament.name         = values["name"];
                    new_tournament.date         = DateTime.Parse(values["date"]);
                    new_tournament.player_size  = Int32.Parse(values["player_size"]);

                    db.tournaments.Add(new_tournament);
                    db.SaveChanges();

                    return new_tournament.id;

                case "OurPlayer":
                    OurPlayer new_player = new OurPlayer();

                    new_player.name = values["name"];
                    new_player.tournament_id = Convert.ToInt64(values["tournament"]);

                    db.ourPlayers.Add(new_player);
                    db.SaveChanges();

                    return new_player.id;

                case "OppTeam":
                    OppTeam new_oppteam = new OppTeam();

                    new_oppteam.name = values["name"];
                    new_oppteam.tournament_id = Convert.ToInt64(values["tournament"]);

                    db.oppTeams.Add(new_oppteam);
                    db.SaveChanges();

                    return new_oppteam.id;

                default:
                    return 0;
            }
        }

        public string ShowList(string type, long id)
        {
            var tables = new Dictionary<string, List<dynamic>>();

            tables.Add("Tournaments", db.tournaments.ToList<dynamic>());
            tables.Add("OppTeams", db.oppTeams.ToList<dynamic>());
            tables.Add("OurPlayers", db.ourPlayers.ToList<dynamic>());

            string return_string = type + ":\n";
            var currtable = (id == 0) ? tables[type] : tables[type].Where(it => it.tournament_id == Convert.ToInt64(id)); 

            if (currtable.Count() == 0)
            {
                return_string += "No values";
                return return_string;
            }

            List<dynamic> columns = new List<dynamic>();

            return_string += "id\t";

            var firstitem = tables[type][0];
            foreach (var property in firstitem.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(string) || property.PropertyType == typeof(int) || property.PropertyType == typeof(DateTime))
                {
                    columns.Add(property);
                    return_string += property.Name + "\t";
                }
            }          
            return_string += "\n";

            foreach (var item in currtable)
            {
                return_string += item.id + "\t";

                foreach (var column in columns)
                {
                    var item_value = column.GetValue(item); 
                    return_string += (item_value is DateTime) ? item_value.ToString("d") : item_value;
                    return_string += "\t";
                }

                return_string += "\n";
            }

            return return_string; 
        }
    }
}
