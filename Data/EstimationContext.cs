﻿using DataBaseConnect.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataBaseConnect.Data
{
    public class EstimationContext : DbContext
    {
        public EstimationContext()
        { 
        }

        public EstimationContext(DbContextOptions<EstimationContext> options) : base(options: options)
        {
        }

        public virtual DbSet<Tournament> tournaments { get; set; }
        public virtual DbSet<OurPlayer> ourPlayers { get; set; }
        public virtual DbSet<OppTeam> oppTeams { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Database=Estimations;Username=postgres;Password=admin");
                //optionsBuilder.UseNpgsql("Host=ec2-54-155-200-16.eu-west-1.compute.amazonaws.com;Port=5432;Username=zecjqmwdwrrggj;Password=2baf5668b882deb570f34920acb63c8b2c89d0516c276d7dd326687b76796c34;Database=d88avthq5pqd1f;SSL Mode=Require");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tournament>().ToTable(name: "tournament");
            modelBuilder.Entity<OurPlayer>().ToTable(name: "ourPlayer");
            modelBuilder.Entity<OppTeam>().ToTable(name: "oppTeam");
        }
    }
}
