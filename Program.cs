﻿using System;
using System.Collections.Generic;
using DataBaseConnect.Data;

namespace DataBaseConnect
{
    class Program
    {
        static void Main(string[] args)
        {
            long id;

            DatabaseContainer db_logic = new DatabaseContainer();
            Console.WriteLine(db_logic.ShowList("Tournaments",0));

            //#region AddTournament
            //Console.WriteLine("Creating new tournament");

            //Dictionary<string, string> Tvalues = new Dictionary<string, string>();
            //foreach (KeyValuePair<string, string> KeyValue in db_logic.GetValuesNames("Tournament"))
            //{
            //    Console.WriteLine(KeyValue.Value);
            //    Tvalues.Add(KeyValue.Key, Console.ReadLine());
            //}
            //id = db_logic.AddItem("Tournament", Tvalues);

            //Console.WriteLine("\n\nAdding players");

            //for (int i = 1; i <= Int32.Parse(Tvalues["player_size"]); i++)
            //{
            //    Dictionary<string, string> Pvalues = new Dictionary<string, string>();
            //    foreach (KeyValuePair<string, string> KeyValue in db_logic.GetValuesNames("OurPlayer"))
            //    {
            //        Console.WriteLine(KeyValue.Value);
            //        Pvalues.Add(KeyValue.Key, Console.ReadLine());
            //    }
            //    Pvalues.Add("tournament", Convert.ToString(id));
            //    db_logic.AddItem("OurPlayer", Pvalues);
            //}

            //Console.WriteLine("\n\nAdding opponent teams");

            //string proceed = "y";
            //while (proceed == "y")
            //{
            //    Dictionary<string, string> teamvalues = new Dictionary<string, string>();
            //    foreach (KeyValuePair<string, string> KeyValue in db_logic.GetValuesNames("OppTeam"))
            //    {
            //        Console.WriteLine(KeyValue.Value);
            //        teamvalues.Add(KeyValue.Key, Console.ReadLine());
            //    }
            //    teamvalues.Add("tournament", Convert.ToString(id));
            //    db_logic.AddItem("OppTeam", teamvalues);

            //    Console.WriteLine("Add another team? [y/n]");
            //    proceed = Console.ReadLine();
            //}
            //#endregion

            #region ShowTournament
            Console.WriteLine("\nEnter tournament id to browse it:");
            id = Convert.ToInt64(Console.ReadLine());

            Console.WriteLine(db_logic.ShowList("OurPlayers", id));

            Console.WriteLine(db_logic.ShowList("OppTeams", id));
            #endregion
        }
    }
}
